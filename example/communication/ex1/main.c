#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
  int fd[2]; // descrittori di file della pipe
  pid_t pid;
  char buf[1];

  pipe(fd);
  pid = fork();
  if(pid > 0){
    printf("Mando un messaggio a mio figlio.\n");
    write(fd[1], "a", 1);
    wait(NULL);
  }else if(pid == 0){
    read(fd[0], buf, 1);
    printf("Mio padre mi ha mandato %c\n", buf[0]);
    return 0;
  }
  printf("Termine.\n");
}
