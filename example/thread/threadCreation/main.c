#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* threadFunction(void *args){
    pthread_t* myTid = (pthread_t *) args;
    if(pthread_equal(*myTid, pthread_self())){
      printf("Si, sono io!\n ");
    }
}

int main(){
  pthread_t tid;
  int rv;

  pthread_create(&tid, NULL, threadFunction, (void * ) &tid);  //  Creo un thread, e gli passo come argomento
                                              
  sleep(3);
  return 0;
}
