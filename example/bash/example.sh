#!/bin/bash

#  Scrivere un programma bash che cerchi in tutti file .c nelle sottodirectory
#  il nome di una funzione.

functionName=$2
fileList=$(find $1 -type f -name *.c)
echo "File .c trovati:"
echo $fileList


echo "FILE contenente $functionName"

for f in $fileList
do
    err=$(cat $f | grep $functionName)
    if [ $? -eq 0 ]
    then
	echo $f
    fi
done

