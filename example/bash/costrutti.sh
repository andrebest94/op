#!/bin/bash
# controllo del flusso

# if-then-else-fi
# valuta exit status di una sequenza di comandi
# in shell UNIX 0 -> True.

# Sintax 1

# if condExpr    -> questa è come se fosse una unica isruzione
# then
#    statement
# fi

# Sintax 2

# if condExpr ; then
#    statements
# fi

# Sintax 3

# if condExpr
# then
#   statements
# else
#   statements
# fi

# Sintax 4

# if condExpr
# then
#   statements
# elif condExrp2
# then
#   statements
# else
#   statements
# fi


# Scrittura condExpr

#Sintax 1

# test param op param

# Sintax 2

# [ param op param ]  -> inserire spazio prima e dopo le parentesi

# param -> possono essere di qualsiasi tipo.
# op    -> esistono vari operatori disponibile. SOno tabulati nei vari formulari.
