#!/bin/bash
# NUmber of login(s) of a specific user

echo -n "User name: "
read user

times = $(who | grep $user | wc -l)

echo "USer $user has $times login(s)"

exit 0
