#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define MAX 4
 
//define two logical values
#define true 1
#define false 0


int turn = 0;
int flag[2] = { false, false};
int count = 0;

void waitRandomTime(int maxtime){
  if(maxtime == 0)
    maxtime = MAX;
  sleep( (int) rand() % maxtime + 1);
} 
void SC(int i){
  waitRandomTime(1);
  printf("(%d) - Sono in sezione critica.\n", i+1);
  if(i == 0){
    count++;
  }else{
    count--;
  }
  printf("\t  count = %d\n", count);
}

void* thread1(void* args){
  int i=0, j=1; //Locally create i, j values for algorithm
  while(true){
    flag[i] = true;
    turn = j;
    while(flag[j] && turn == j);
    SC(0);
    flag[i] = false;
    //waitRandomTime(2);
  }
}

void* thread2(void* args){
  int i=0, j=1;  //Locally create i, j values for algorithm
  for(int k=0; k < 4; k++){
    flag[j] = true; 
    turn = i;
    while(flag[i] && turn == i);
    SC(1);
    flag[j] = false;
    waitRandomTime(2);
  }
}


int main(){
  srand(NULL);
  pthread_t tids[2];
  printf(" == TEST sezioni critiche == \n");
  pthread_create(&tids[0], NULL, thread1, NULL);
  pthread_create(&tids[1], NULL, thread2, NULL);
  sleep(3);
  pthread_exit(NULL);
}
