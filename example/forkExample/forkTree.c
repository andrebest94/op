#include <unistd.h>
#include <stdio.h>

int main(){
   pid_t pid;

   pid = fork();

   switch(pid){
   	case 0:
		//son
		fprintf(stdout, "I'm the son");
	break;
	case -1:
		fprintf(stderr, "Errore nella generazione del processo");
	break;
	default:
		//father
		wait(NULL);
		fprintf(stdout, "My son just finished");
	break;
   }
}
