#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

int main(){
  pid_t pid;
  int i;
  pid = fork();
  if(pid > 0){
    for(i=0; i < 10; i++){
      printf("P");
    }
    wait((int*) 0);
  }
  else{
    for(i=0; i < 10; i++){
      printf("F");
    }
  }
  return 0;
}
