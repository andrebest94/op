#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
  int n = atoi(argv[1]);
  int factorial = 1;
  while(n != 0){
    factorial *= n;
    n--;
  }
  printf("Fattoriale di %s: %d\n",argv[1],factorial);
  return 0;
}
