#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int a, b;

void handler(int sig);

int main(int argc, char* argv[]){
  a = atoi(argv[1]);
  b = atoi(argv[2]);
  signal(SIGUSR2, handler);
  signal(SIGUSR1, handler);
  while(1){pause();}
  return 0;
}

void handler(int sig){
  if(sig == SIGUSR2){
    printf("a+b: %d\n", a + b);
  }else if(sig == SIGUSR1){
    printf("a-b: %d\n", a - b);
  }else if(sig == SIGINT){
    exit(0);
  }
}
