#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>

void handler(){
  printf("Ricevuto un segnale\n");
  return;
}

void child1(){
  int fd, i;
  char buffer[1];
  fd = open("son1.txt", O_RDONLY);
  for(i=0; i < 50; i++){
    read(fd, buffer, 1);
    printf("%c", buffer[0]);
  }
  printf("\n");
  close(fd);
}

void child2(){
  int fd, i;
  char buffer[1];
  fd = open("son2.txt", O_RDONLY);
  for(i=0; i < 50; i++){
    read(fd, buffer, 1);
    printf("%c", buffer[0]);
  }
  printf("\n");
  close(fd);
  sleep(5);
}


int main(){
  int i;
  pid_t pids[2];
  signal(SIGCHLD, handler);
  for(i=0; i < 2; i++){
    pids[i] = fork();
    if(pids[i] > 0){
      printf("PID[%d] = %d\n", i, pids[i]);
    }else if(pids[i] == 0){
        // son
        switch(i){
          case 0:
            child1();
          break;
          case 1:
            child2();
          break;
        }
        exit(0);
    }
  }
  for(i=0; i < 2; i++){
    printf("Aspetto\n");
    pause();
    printf("Ho ricervuto un segnale\n");
  }
  return 0;
}
