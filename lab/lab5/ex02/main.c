#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

int main(int argc, char const *argv[]) {
  pid_t pid = atoi(argv[1]);
  if(!strcmp(argv[2], "somma")){
    kill(pid, SIGUSR2);
  }else if(!strcmp(argv[2], "diff")){
    kill(pid, SIGUSR1);
  }else if(!strcmp(argv[2], "fine")){
    kill(pid, SIGINT);
  }else{
    printf("Wrong command\n");
  }
  return 0;
}
