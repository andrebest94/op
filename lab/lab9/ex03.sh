#!/bin/bash

ping -c 1 "www.google.com" | grep "1 received"
if [ $? -eq 0 ]
then
  echo "connected"
else
  echo "Not connected"
fi
