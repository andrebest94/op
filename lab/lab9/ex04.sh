#!/bin/bash

for f in $(find $1 -type f)
do
  name=$(basename $f)
  nameUp=$(echo $name| tr [a-z] [A-Z])
  pathNew=$(dirname $f)
  mv $f $pathNew/$nameUp
done
