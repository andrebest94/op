#include <stdio.h>
#include <string.h>

int main(){
  char buffer[100];
  int i;
  scanf("%s", buffer);
  for(i = 0 ; i < strlen(buffer); i++){
    if(buffer[i] >= 97 )
      buffer[i] -= 32;
  }
  printf("%s\n", buffer);
}
