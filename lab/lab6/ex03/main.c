#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/wait.h>

#define MAX 10
#define dbg
void childFunction(int index, int fd){
  double returnValue = 0.0;
  returnValue = exp(index)/index;
  write(fd, &returnValue, sizeof(double));
  return;
}

int main(int argc, char *argv[]){
  int i, n;
  double x=0, value = 0;
  int fd[2];
  pid_t pid;
  pipe(fd);
  n = atoi(argv[1]);
  if(n > MAX)
    exit(0);
  for(i = 0; i < n; i++){
    pid = fork();
    if(pid == 0){
      childFunction(i+1, fd[1]);
      return 0;
    }
  }
  for(i=0; i<n; i++){
    read(fd[0], &x, sizeof(double));
    value += x;
    #ifdef dbg
    printf("%.3f \n", x);
    #endif
  }
  printf("value = %.2f", value);
  return 0;
}
