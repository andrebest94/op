#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>

void handler(){
  exit(0);
}

int main(){
  pid_t pid;
  int channel[2], fd;
  char buffer[100];
  pipe(channel);
  pid = fork();
  if(pid > 0){
    signal(SIGCHLD, handler);
    close(channel[1]);
    while(1){
      read(channel[0], buffer, 100);
      printf("%s\n", buffer);
    }
  }else if(pid == 0){
    close(channel[0]);
    fd = open("file.txt", O_RDONLY);
    while(read(fd, buffer, 100) != 0){
      write(channel[1], buffer, 100);
    }
    exit(0);
  }else if(pid == -1){
    printf("Errore nella generazione del processo");
  }
  return 0;
}
