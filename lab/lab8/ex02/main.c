#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

typedef struct{
  int sx, dx;
}interval_t;

int n, notPrime = 0;

void* threadFunction(void* args){
  interval_t *myInterval;
  int sx, dx, i;
  myInterval = args;
  sx = myInterval->sx;
  dx = myInterval->dx;
  for(i = sx; i <= dx; i++){
    if(n % i == 0){
      printf("%d divisibile per %d.\n", n, i);
      notPrime = 1;
      pthread_exit(NULL);
    }
  }
  pthread_exit(NULL);
}

int main(int argc, char *argv[]){
  int p, i, err;
  pthread_t* tid;
  interval_t *intervals;
  time_t start, end;
  n = atoi(argv[1]);
  p = atoi(argv[2]);
  start = time(NULL);
  printf("Checking if %d is prime with %d threads.\n", n, p);
  tid = malloc(p * sizeof(pthread_t));
  intervals = malloc(p * sizeof(interval_t));

  for(i=0; i < p; i++){
    intervals[i].sx = i+1;
    intervals[i].dx = i+1;
     err = pthread_create(&tid[i], NULL, threadFunction, (void *) &intervals[i]);
    if(err != 0){
      printf("Thread non creato.\n");
      exit(1);
    }
  }
  for(i = 0; i <p; i++){
    pthread_join(tid[i], NULL);
    if(notPrime != 0){
      end = time(NULL);
      printf("%d is prime.\n", n);
      printf("================================\n");
      printf("Execution time: %ld s\n", end - start);
      printf("================================\n");
      exit(1);
    }
  }
  end = time(NULL);
  printf("%d is prime.\n", n);
  printf("================================\n");
  printf("Execution time: %ld s\n", end - start);
  printf("================================\n");
  exit(0);
}
