#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


#define BUFFERSIZE 1024

int main(int argc, char *argv[]){
  int nr, nw, fdr, fdw;
  char buf[BUFFERSIZE];

  fdr = open(argv[1], O_RDONLY);
  fdw = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);

  if( fdr == -1 ){
    fprintf(stderr, "Error in openening file 1");
    exit(1);
  }

  if(fdw == -1){
    fprintf(stderr, "Error in opening file 2");
    exit(1);
  }

  while((nr = read(fdr, buf, BUFFERSIZE)) > 0){
    nw = write(fdw, buf, nr);
    if(nr != nw){
      fprintf(stderr, "Error, Read: %d, Write: %d", nr, nw);
      exit(1);
    }
  }

  if( nr < 0 ){
    fprintf(stderr, "Write error");
  }

  close(fdr);
  close(fdw);
  return 0;
}

